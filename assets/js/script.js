console.log("Hello World.");

/* create trainer object */
let trainer = {
    name: 'Jed',
    age: 28,
    pokemonOnHand: 'Bulbasaur',
}

/* access the "trainer" object properties using dot and square bracket notation */
console.log(trainer);
console.log(trainer.pokemonOnHand);
console.log(trainer['name']);

/* object constructor */
function createPokemon(name) {
    this.name = name;
    this.health = 100;
    this.tackle = function(target) {
        console.log(`${this.name} tackled ${target.name} `);
        target.health -= 20;
        console.log(`${target.name}'s health is now ${target.health}`);
    }
};

/* create /instantiate several pokemon using the " constructor" function */
let absol = new createPokemon("Absol");
let groudon = new createPokemon("Groudon");

/* pokemon objects interact with each other by calling "tackle" method. */
absol.tackle(groudon);